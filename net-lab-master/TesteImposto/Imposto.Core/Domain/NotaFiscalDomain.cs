﻿
using Imposto.Core.Commons;
using Imposto.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class NotaFiscalDomain
    {
        IUnitOfWork<NotaFiscal> UnitOfWork { get; set; }

        public NotaFiscalDomain()
        {
            this.UnitOfWork = new NotaFiscalRepository();
        }

        public int SaveWithProcedureNF(NotaFiscal Obj)
        {
            int ret = 0;

            try
            {
                List<SqlParameter> Params = new List<SqlParameter>();

                var pId = new SqlParameter("pId", Obj.Id);
                Params.Add(pId);

                var pNumeroNotaFiscal = new SqlParameter("pNumeroNotaFiscal", Obj.NumeroNotaFiscal);
                Params.Add(pNumeroNotaFiscal);

                var pSerie = new SqlParameter("pSerie", Obj.Serie);
                Params.Add(pSerie);

                var pNomeCliente = new SqlParameter("pNomeCliente", Obj.NomeCliente);
                Params.Add(pNomeCliente);

                var pEstadoDestino = new SqlParameter("pEstadoDestino", Obj.EstadoDestino);
                Params.Add(pEstadoDestino);

                var pEstadoOrigem = new SqlParameter("pEstadoOrigem", Obj.EstadoOrigem);
                Params.Add(pEstadoOrigem);

                ret = this.UnitOfWork.SaveWithProcedure("P_NOTA_FISCAL", Params);



            }
            catch (Exception ex)
            {
                return 0;
            }

            return ret;
        }

        public int SaveWithProcedureNFI(NotaFiscalItem Obj)
        {
            int ret = 0;

            try
            {
                List<SqlParameter> Params = new List<SqlParameter>();

                var pId = new SqlParameter("pId", Obj.Id);
                Params.Add(pId);

                var pIdNotaFiscal = new SqlParameter("pIdNotaFiscal", Obj.IdNotaFiscal);
                Params.Add(pIdNotaFiscal);

                var pCfop = new SqlParameter("pCfop", Obj.Cfop);
                Params.Add(pCfop);

                var pTipoIcms = new SqlParameter("pTipoIcms", Obj.TipoIcms);
                Params.Add(pTipoIcms);

                var pBaseIcms = new SqlParameter("pBaseIcms", Obj.BaseIcms.ToString().Replace(",", "."));
                Params.Add(pBaseIcms);

                var pAliquotaIcms = new SqlParameter("pAliquotaIcms", Obj.AliquotaIcms.ToString().Replace(",", "."));
                Params.Add(pAliquotaIcms);

                var pValorIcms = new SqlParameter("pValorIcms", Obj.ValorIcms.ToString().Replace(",", "."));
                Params.Add(pValorIcms);

                var pNomeProduto = new SqlParameter("pNomeProduto", Obj.NomeProduto);
                Params.Add(pNomeProduto);

                var pCodigoProduto = new SqlParameter("pCodigoProduto", Obj.CodigoProduto);
                Params.Add(pCodigoProduto);

                var pBaseIPI = new SqlParameter("pBaseIPI", Obj.BaseIPI.ToString().Replace(",", "."));
                Params.Add(pBaseIPI);

                var pValorIPI = new SqlParameter("pValorIPI", Obj.ValorIPI.ToString().Replace(",", "."));
                Params.Add(pValorIPI);

                var pAliquotaIPI = new SqlParameter("pAliquotaIPI", Obj.AliquotaIPI.ToString().Replace(",", "."));
                Params.Add(pAliquotaIPI);

                var pDesconto = new SqlParameter("pDesconto", Obj.Desconto.ToString().Replace(",", "."));
                Params.Add(pDesconto);

                ret = this.UnitOfWork.SaveWithProcedure("P_NOTA_FISCAL_ITEM", Params);



            }
            catch (Exception ex)
            {
                return 0;
            }

            return ret;
        }

        public bool EmitirNotaFiscal(Pedido pedido)
        {
            try
            {
                NotaFiscal notaFiscal = new NotaFiscal();
                notaFiscal.NumeroNotaFiscal = 99999;
                notaFiscal.Serie = new Random().Next(Int32.MaxValue);
                notaFiscal.NomeCliente = pedido.NomeCliente;

                notaFiscal.EstadoDestino = pedido.EstadoDestino.ToUpper();
                notaFiscal.EstadoOrigem = pedido.EstadoOrigem.ToUpper();

                foreach (PedidoItem itemPedido in pedido.ItensDoPedido)
                {
                    NotaFiscalItem notaFiscalItem = new NotaFiscalItem();

                    notaFiscalItem = SearchCFOP(notaFiscal);

                    notaFiscalItem = SearchIcms(notaFiscal);

                    if (notaFiscalItem.Cfop == "6.009")
                    {
                        notaFiscalItem.BaseIcms = itemPedido.ValorItemPedido * 0.90; //redução de base
                    }
                    else
                    {
                        notaFiscalItem.BaseIcms = itemPedido.ValorItemPedido;
                    }
                    notaFiscalItem.ValorIcms = notaFiscalItem.BaseIcms * notaFiscalItem.AliquotaIcms;

                    notaFiscalItem.BaseIPI = itemPedido.ValorItemPedido;

                    if (itemPedido.Brinde)
                    {
                        notaFiscalItem.TipoIcms = "60";
                        notaFiscalItem.AliquotaIcms = 0.18;
                        notaFiscalItem.ValorIcms = notaFiscalItem.BaseIcms * notaFiscalItem.AliquotaIcms;
                        notaFiscalItem.AliquotaIPI = 0;
                        notaFiscalItem.ValorIPI = itemPedido.ValorItemPedido * 0;

                    }
                    else
                    {

                        notaFiscalItem.AliquotaIPI = 0.10;
                    }

                    notaFiscalItem.ValorIPI = notaFiscalItem.BaseIPI * notaFiscalItem.AliquotaIPI;

                    notaFiscalItem.NomeProduto = itemPedido.NomeProduto;
                    notaFiscalItem.CodigoProduto = itemPedido.CodigoProduto;

                    notaFiscalItem.Desconto = ApplyDiscount(notaFiscal.EstadoDestino);

                    notaFiscal.ItensDaNotaFiscal.Add(notaFiscalItem);
                }

                String FileName = $"{notaFiscal.NumeroNotaFiscal}-{notaFiscal.Serie}-{notaFiscal.NomeCliente.Replace(" ", "_")}";

                UtilsXML _UtlilsXML = new UtilsXML();

                if (_UtlilsXML.ConverterParaXML(notaFiscal, FileName))
                {
                    //Salvar no banco
                    var ret = SaveWithProcedureNF(notaFiscal);

                    foreach (var item in notaFiscal.ItensDaNotaFiscal)
                    {
                        item.IdNotaFiscal = ret;

                        SaveWithProcedureNFI(item);

                    }
                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public double ApplyDiscount(string Obj)
        {
            //Lista de estado com desconto
            List<string> DiscountState = new List<string>
            {
                "SP",
                "RJ",
                "MG",
                "ES"
            };


            if (DiscountState.Where(w => w.Equals(Obj.ToUpper())).FirstOrDefault() != null)
            {
                return (double)0.10;
            }
            else
            {
                return (double)0;
            }

        }

        public NotaFiscalItem SearchCFOP(NotaFiscal notaFiscal)
        {

            NotaFiscalItem notaFiscalItem = new NotaFiscalItem();

            if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "RJ"))
            {
                notaFiscalItem.Cfop = "6.000";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "PE"))
            {
                notaFiscalItem.Cfop = "6.001";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "MG"))
            {
                notaFiscalItem.Cfop = "6.002";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "PB"))
            {
                notaFiscalItem.Cfop = "6.003";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "PR"))
            {
                notaFiscalItem.Cfop = "6.004";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "PI"))
            {
                notaFiscalItem.Cfop = "6.005";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "RO"))
            {
                notaFiscalItem.Cfop = "6.006";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "SE"))
            {
                notaFiscalItem.Cfop = "6.007";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "TO"))
            {
                notaFiscalItem.Cfop = "6.008";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "SE"))
            {
                notaFiscalItem.Cfop = "6.009";
            }
            else if ((notaFiscal.EstadoOrigem == "SP") && (notaFiscal.EstadoDestino == "PA"))
            {
                notaFiscalItem.Cfop = "6.010";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "RJ"))
            {
                notaFiscalItem.Cfop = "6.000";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "PE"))
            {
                notaFiscalItem.Cfop = "6.001";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "MG"))
            {
                notaFiscalItem.Cfop = "6.002";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "PB"))
            {
                notaFiscalItem.Cfop = "6.003";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "PR"))
            {
                notaFiscalItem.Cfop = "6.004";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "PI"))
            {
                notaFiscalItem.Cfop = "6.005";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "RO"))
            {
                notaFiscalItem.Cfop = "6.006";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "SE"))
            {
                notaFiscalItem.Cfop = "6.007";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "TO"))
            {
                notaFiscalItem.Cfop = "6.008";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "SE"))
            {
                notaFiscalItem.Cfop = "6.009";
            }
            else if ((notaFiscal.EstadoOrigem == "MG") && (notaFiscal.EstadoDestino == "PA"))
            {
                notaFiscalItem.Cfop = "6.010";
            }
            else
            {
                //CFOP Generico Quando o estado não esta na lista de seleção.
                notaFiscalItem.Cfop = "9.999";
            }

            return notaFiscalItem;

        }

        public NotaFiscalItem SearchIcms(NotaFiscal notaFiscal)
        {

            NotaFiscalItem notaFiscalItem = new NotaFiscalItem();

            if (notaFiscal.EstadoDestino == notaFiscal.EstadoOrigem)
            {
                notaFiscalItem.TipoIcms = "60";
                notaFiscalItem.AliquotaIcms = 0.18;
            }
            else
            {
                notaFiscalItem.TipoIcms = "10";
                notaFiscalItem.AliquotaIcms = 0.17;
            }

            return notaFiscalItem;
        }
    }
}
