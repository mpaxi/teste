﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Infra.Mapping
{
    public class NotaFiscalItemMapping : EntityTypeConfiguration<NotaFiscalItem>, IMapping
    {
        public NotaFiscalItemMapping()
        {
            this.ToTable("NotaFiscalItem");
            this.HasKey(x => x.Id);
            this.Property(x => x.Id);
            this.Property(x => x.IdNotaFiscal).IsRequired();
            this.Property(x => x.Cfop).IsRequired();
            this.Property(x => x.TipoIcms).IsRequired();
            this.Property(x => x.BaseIcms).IsRequired();
            this.Property(x => x.AliquotaIcms).IsRequired();
            this.Property(x => x.ValorIcms).IsRequired();
            this.Property(x => x.NomeProduto).IsRequired();
            this.Property(x => x.CodigoProduto).IsRequired();

            // Relationships
            this.HasRequired(t => t.NotaFiscal)
                .WithMany(t => t.ItensDaNotaFiscal)
                .HasForeignKey(d => d.IdNotaFiscal);
        }
    }
}
