﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Infra.Mapping
{
    public class NotaFiscalMapping : EntityTypeConfiguration<NotaFiscal>, IMapping
    {
        public NotaFiscalMapping()
        {
            this.ToTable("NotaFiscal");
            this.HasKey(x => x.Id);
            this.Property(x => x.Id);
            this.Property(x => x.NumeroNotaFiscal).IsRequired();
            this.Property(x => x.Serie).IsRequired();
            this.Property(x => x.NomeCliente).IsRequired().HasMaxLength(1024);
            this.Property(x => x.EstadoOrigem).IsRequired().HasMaxLength(2).IsFixedLength();
            this.Property(x => x.EstadoDestino).IsRequired().HasMaxLength(2).IsFixedLength();
            this.Property(x => x.EstadoDestino).IsRequired().HasMaxLength(2).IsFixedLength();

        }
    }
}
