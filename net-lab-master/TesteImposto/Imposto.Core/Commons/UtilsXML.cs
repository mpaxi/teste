﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

namespace Imposto.Core.Commons
{
    public class UtilsXML
    {

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Obj"></param>

        public bool ConverterParaXML<T>(T Obj, string FileName)
        {

            //string propertyFile = @"C:\temp\"; 
            string propertyFile = ConfigurationManager.AppSettings["PathXML"].ToString();
            string propertyFolder = propertyFile.Substring(0, propertyFile.LastIndexOf("\\") + 1);
            string newXML = propertyFolder + $"{FileName}.xml";

            if (Obj == null) { return false; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(Obj.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, Obj);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(newXML);
                    stream.Close();
                }

                return true;
            }


            catch (Exception ex)
            {
                ex.Message.ToString();
                //Log exception here
                return false;
            }
        }
    }
}
