-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE P_DRE_BY_CFOP

AS
BEGIN

	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Cfop, 
	sum(BaseIcms) as 'BaseIcms', 
	sum(ValorIcms) as 'ValorIcms', 
	sum(BaseIPI) as 'BaseIPI' ,
	sum(ValorIPI) as 'ValorIPI'
	from notafiscalitem
	group by Cfop

END
GO
