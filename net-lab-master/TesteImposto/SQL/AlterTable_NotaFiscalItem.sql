ALTER TABLE notafiscalitem
ADD BaseIPI decimal(18,5);

ALTER TABLE notafiscalitem
ADD ValorIPI decimal(18,5);

ALTER TABLE notafiscalitem
ADD AliquotaIPI decimal(18,5);

ALTER TABLE notafiscalitem
ADD Desconto decimal(18,5);


