﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imposto.Core.Domain;

namespace UnitTestProject1
{
    [TestClass]
    public class NotaFiscalTest
    {
        NotaFiscalDomain notaFiscalDomain = new NotaFiscalDomain();

        [TestMethod]
        public void TestApplyDiscount()
        {

            var ret = notaFiscalDomain.ApplyDiscount("SP");

            Assert.AreEqual(0.10, ret);
        }

        [TestMethod]
        public void TestSearchCFOP()
        {
            NotaFiscal notaFiscal = new NotaFiscal();

            notaFiscal.EstadoOrigem = "SP";
            notaFiscal.EstadoDestino = "RJ";

            var ret = notaFiscalDomain.SearchCFOP(notaFiscal);

            Assert.AreEqual("6.000", ret.Cfop);
        }

        [TestMethod]
        public void TestSearchIcms()
        {
            NotaFiscal notaFiscal = new NotaFiscal();

            notaFiscal.EstadoOrigem = "SP";
            notaFiscal.EstadoDestino = "RJ";

            var ret = notaFiscalDomain.SearchIcms(notaFiscal);

            Assert.AreEqual("10", ret.TipoIcms);
        }


        [TestMethod]
        public void TestEmitirNotaFiscal()
        {
            Pedido pedido = new Pedido();

            pedido.EstadoOrigem = "RJ";
            pedido.EstadoDestino = "SP";
            pedido.NomeCliente = "Test Murilo Paxi";

            for (int i = 0; i < 3; i++)
            {
                PedidoItem pedidoItem = new PedidoItem();
                pedidoItem.CodigoProduto = $"Codigo do Produto-{1}";
                pedidoItem.NomeProduto = $"Produto-{1}";
                pedidoItem.ValorItemPedido = i;

                if (i == 2)
                {
                    pedidoItem.Brinde = true;
                }
                else
                {
                    pedidoItem.Brinde = false;
                }

                pedido.ItensDoPedido.Add(pedidoItem);

            }

            var ret = notaFiscalDomain.EmitirNotaFiscal(pedido);

            Assert.AreEqual(true, ret);
        }


    }
}
