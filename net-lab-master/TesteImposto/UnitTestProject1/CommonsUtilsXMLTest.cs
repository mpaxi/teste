﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imposto.Core.Domain;
using Imposto.Core.Commons;

namespace UnitTestProject1
{
    [TestClass]
    public class CommonsUtilsXMLTest
    {
        [TestMethod]
        public void TestConverterParaXML()
        {
            UtilsXML utilsXML = new UtilsXML();

            NotaFiscal notaFiscal = new NotaFiscal();

            notaFiscal.EstadoOrigem = "RJ";
            notaFiscal.EstadoDestino = "SP";
            notaFiscal.NomeCliente = "Test Murilo Paxi";
            notaFiscal.NumeroNotaFiscal = 999999;
            notaFiscal.Serie = 1234;

            for (int i = 0; i < 3; i++)
            {
                NotaFiscalItem notaFiscalItem = new NotaFiscalItem();
                notaFiscalItem.IdNotaFiscal = 1;
                notaFiscalItem.CodigoProduto = $"Codigo do Produto-{1}";
                notaFiscalItem.NomeProduto = $"Produto-{1}";
                notaFiscalItem.AliquotaIcms = i;
                notaFiscalItem.AliquotaIPI = i;
                notaFiscalItem.BaseIcms = i;
                notaFiscalItem.BaseIPI = i;
                notaFiscalItem.Cfop = i.ToString();
                notaFiscalItem.Desconto = i;
                notaFiscalItem.TipoIcms = i.ToString();
                notaFiscalItem.ValorIcms = i;
                notaFiscalItem.ValorIPI = i;

                notaFiscal.ItensDaNotaFiscal.Add(notaFiscalItem);

            }

            String FileName = $"{notaFiscal.NumeroNotaFiscal}-{notaFiscal.Serie}-{notaFiscal.NomeCliente.Replace(" ", "_")}";

            var ret = utilsXML.ConverterParaXML(notaFiscal, FileName);

            Assert.AreEqual(true, ret);

        }
    }
}
