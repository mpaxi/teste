﻿using System;

namespace TesteVogal
{
    public interface IStream
    {
        char GetNext();

        Boolean HasNext();
    }

    public class StringStream : IStream
    {
        public String stream;

        private int indexOf = 0;

        public StringStream(String stream)
        {
            this.stream = stream;
        }

        //Retorna próximo caracter do stream
        public char GetNext()
        {
            return this.stream[indexOf++];
        }

        //Valida se existem mais caracteres
        public Boolean HasNext()
        {
            return (this.stream.Length > indexOf);
        }
    }

    public class ProcuraVogal
    {
        public char PrimeiroChar(IStream input)
        {
            char cc;
            int index = 0;
            int found;
            char[] chars = new char[((StringStream)(input)).stream.Length];
            short[] Repetidos = new short[((StringStream)(input)).stream.Length];

            while (input.HasNext())
            {
                cc = input.GetNext();
                found = RetornaIndex(cc, chars);

                if (found >= 0)
                    Repetidos[found]++;
                else
                {
                    chars[index] = cc;
                    Repetidos[index]++;
                    index++;
                }
            }

            //Seleciona o primeiro caracter não repetido
            int firstCharIndex = FirstIndex(Repetidos);

            //Valida se encontrou
            if (firstCharIndex < 0)
                throw new Exception("Não existe char repetido.");

            return chars[firstCharIndex];
        }

        /// <summary>
        /// Retorna o índice do primeiro caracter que se existe apenas uma vez
        /// </summary>
        /// <param name="qtdRepetidos"></param>
        /// <returns></returns>
        private static int FirstIndex(short[] qtdRepetidos)
        {
            for (int i = 0; i < qtdRepetidos.Length; i++)
            {
                if (qtdRepetidos[i] == 1)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Pesquisa do caracter
        /// </summary>
        /// <param name="cc"></param>
        /// <param name="chars"></param>
        /// <returns></returns>
        private static int RetornaIndex(char cc, char[] chars)
        {
            for (int i = 0; i < chars.Length; i++)
            {
                if (cc == chars[i])
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
