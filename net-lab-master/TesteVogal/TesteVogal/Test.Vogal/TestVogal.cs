﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TesteVogal;

namespace Test.Vogal
{
    [TestClass]
    public class TestVogal
    {
        [TestMethod]
        public void TestVogalValid()
        {
            StringStream stream = new StringStream("aAbBABac");
            ProcuraVogal procuraVogal = new ProcuraVogal();
            char resultado;
            try
            {
                resultado = procuraVogal.PrimeiroChar(stream);
            }
            catch (System.Exception)
            {
                resultado = ' ';
            }
            Assert.IsTrue(resultado != ' ');
        }

        [TestMethod]
        public void TestVogalInvalid()
        {
            StringStream stream = new StringStream("aaaaaaaaa");
            ProcuraVogal procuraVogal = new ProcuraVogal();
            char resultado;
            try
            {
                resultado = procuraVogal.PrimeiroChar(stream);
            }
            catch (System.Exception)
            {
                resultado = ' ';
            }
            Assert.IsTrue(resultado == ' ');
        }
    }
}
